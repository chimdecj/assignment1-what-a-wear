package WhatAWear.chimdeecj.com.WhatWear;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {

    Button btnList;
    ImageButton btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WhatAWear.chimdeecj.com.WhatWear.MainActivity.this, ClothesList.class);
                startActivity(intent);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WhatAWear.chimdeecj.com.WhatWear.MainActivity.this, AddItem.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Энэхүү процетур нь AddItem болон ClothesList классруу intent шиднэ.
     */

    private void init(){
        btnAdd = (ImageButton) findViewById(R.id.btnAdd);
        btnList = (Button) findViewById(R.id.btnList);
    }
}
